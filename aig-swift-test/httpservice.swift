//
//  httpservice.swift
//  aig-swift-test
//
//  Created by MIKIO ODA on 2020/04/17.
//  Copyright © 2020 MIKIO ODA. All rights reserved.
//

import Foundation

open class HTTPService {
    
    public struct Request : Codable {
        var code : Int
        var message : String
    }
    
    public struct Response : Codable {
        var code : Int
        var message : String
    }
    
    let session =  URLSession.shared
    let encoder = JSONEncoder()
    
    init() {
        encoder.outputFormatting = .prettyPrinted
    }
    
    private func setHeader(method: String,request: inout URLRequest) -> Void {
        request.httpMethod = method
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
    }
    
    open func get(url: URL,callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        setHeader(method:"GET", request:&request)
        session.dataTask(with: request, completionHandler: callback).resume()
    }
    
    open func post(url: URL,body: Data,callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        setHeader(method:"POST" ,request:&request)
        request.httpBody = body
        session.dataTask(with: request, completionHandler: callback).resume()
    }
    
    open func put(url: URL,body: Data,callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        setHeader(method:"PUT", request:&request)
        request.httpBody = body
        session.dataTask(with: request, completionHandler: callback).resume()
    }
    
    public func delete(url: URL,callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        setHeader(method:"DELETE", request:&request)
        session.dataTask(with: request, completionHandler: callback).resume()
    }
    
}
