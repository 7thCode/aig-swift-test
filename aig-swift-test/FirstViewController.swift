//
//  FirstViewController.swift
//  aig-swift-test
//
//  Created by MIKIO ODA on 2020/04/15.
//  Copyright © 2020 MIKIO ODA. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet var field: UITextField!
    var service: HTTPService?
    var url: URL?
    
    let encoder = JSONEncoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        service = HTTPService()
        
        //   url = URL(string: "https://seventh-code.com/test")
        url = URL(string: "http://localhost:3000/test")
        encoder.outputFormatting = .prettyPrinted
    }
    
    @IBAction
    func Get(_ sender: Any) {
        service!.get(url:url!) {(data, response, error) in
            if error == nil {
                guard let data = data else { return }
                let json = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                DispatchQueue.main.async { // Correct
                    self.field.text = json?.message ?? "GET error"
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    @IBAction
    func Post(_ sender: Any) {
        do {
            let params:HTTPService.Request = HTTPService.Request(code:1, message: "POST!")
            let body: Data = try encoder.encode(params)
            service!.post(url:url!, body: body) {(data, response, error) in
                if error == nil {
                    guard let data = data else { return }
                    let json = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                    DispatchQueue.main.async { // Correct
                        self.field.text = json?.message ?? "POST error"
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction
    func Put(_ sender: Any) {
        do {
            let params:HTTPService.Request = HTTPService.Request(code:1, message: "PUT!")
            let body: Data = try encoder.encode(params)
            service!.put(url:url!, body: body) {(data, response, error) in
                if error == nil {
                    guard let data = data else { return }
                    let json = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                    DispatchQueue.main.async { // Correct
                        self.field.text = json?.message ?? "PUT error"
                    }
                } else {
                    print(error!.localizedDescription)
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction
    func Delete(_ sender: Any) {
        service!.delete(url:url!) {(data, response, error) in
            if error == nil {
                guard let data = data else { return }
                let json = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                DispatchQueue.main.async { // Correct
                    self.field.text = json?.message ?? "DELETE error"
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
}

