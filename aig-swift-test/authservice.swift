//
//  authservice.swift
//  aig-swift-test
//
//  Created by MIKIO ODA on 2020/04/17.
//  Copyright © 2020 MIKIO ODA. All rights reserved.
//

import Foundation

class AuthService: HTTPService {
    
    public struct Content : Codable {
        var content: String
    }
    
    var target: String = ""
    
    func login(username: String, password: String, callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        do {
            let endpoint: URL? = URL(string: self.target + "/auth/local/login")
            let content: String = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}"
            let params: Content = Content(content: content)
            let body: Data = try encoder.encode(params)
            post(url: endpoint!, body: body, callback: callback)
        } catch {
            callback(nil, nil, error)
        }
    }
    
    func logout(callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        let endpoint: URL? = URL(string: self.target + "/auth/logout")
        get(url: endpoint!, callback: callback)
    }
    
    func login_totp(username: String, password: String, code: String,callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        do {
            let endpoint: URL? = URL(string: self.target + "/auth/local/login_totp")
            let content: String = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\", \"code\":\"" + code + "\" }"
            let params: Content = Content(content: content)
            let body: Data = try encoder.encode(params)
            post(url: endpoint!, body: body, callback: callback)
        } catch {
            callback(nil, nil, error)
        }
    }
    
    func regist(username: String, password: String, metadata: String, callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        do {
            let endpoint: URL? = URL(string: self.target + "/auth/local/register")
            let content: String = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\", \"metadata\":\"" + metadata + "\" }"
            let params: Content = Content(content: content)
            let body: Data = try encoder.encode(params)
            post(url: endpoint!, body: body, callback: callback)
        } catch {
            callback(nil, nil, error)
        }
    }
    
    func password(username: String, password: String, callback: @escaping (Data?, URLResponse?, Error?) -> Void) -> Void {
        do {
            let endpoint: URL? = URL(string: self.target + "/auth/local/password")
            let content: String = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}"
            let params: Content = Content(content: content)
            let body: Data = try encoder.encode(params)
            post(url: endpoint!, body: body, callback: callback)
        } catch {
            callback(nil, nil, error)
        }
    }
    
}


