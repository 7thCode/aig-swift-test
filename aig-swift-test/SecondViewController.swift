//
//  SecondViewController.swift
//  aig-swift-test
//
//  Created by MIKIO ODA on 2020/04/15.
//  Copyright © 2020 MIKIO ODA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet var field: UILabel!
    @IBOutlet var address: UITextField!
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    
    var service = AuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func Login(_ sender: Any) {
        service.target = address.text ?? "http://localhost:3000"
        service.login(username: self.username.text!, password: self.password.text!) {(data, response, error) in
            if error == nil {
                guard let data = data else { return }
                let response = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                DispatchQueue.main.async { // Correct
                    if response!.code == 0 {
                        self.field.text = "login."
                    } else {
                        self.field.text = response?.message ?? "Login error"
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
            
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        service.target = address.text ?? "http://localhost:3000"
        service.logout() {(data, response, error) in
            if error == nil {
                guard let data = data else { return }
                let response = try? JSONDecoder().decode(HTTPService.Response.self, from: data)
                DispatchQueue.main.async { // Correct
                    if response!.code == 0 {
                        self.field.text = "logout."
                    } else {
                        self.field.text = response?.message ?? "Login error"
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
        }
    }
}

